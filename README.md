# alicetokenacc_build

scripts for building alicetokenacc package

## Getting started

the source of alicetokenacc should be found in `alicetokenacc` directory

Recipe for building rpms:

1. Build the singularity container used for build environment
./build_sing_cont_el{7..9}

2. run building script for rpms
./make_rpm{7..9}

rpms will be found in `packages` directory

